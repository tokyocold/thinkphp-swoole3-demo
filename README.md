##thinkphp6.0+thinkphp-swoole3扩展demo

> 运行环境要求PHP7.1+

> swoole4.5+


####注意事项:
1. SwooleBoot在init与managerStart中启动是有区别的,请仔细阅读注释内容.
2. Timer已经完成了对全客户端的文本Ping操作,queue消息投递参照实现即可.
3. swoole的编程有别于以往的php-fpm模式开发,请仔细阅读swoole官方后再进行开发.关于基础知识问题将不再做回答.
4. 该demo未经实际项目测试,造成的bug或其它影响请自行斟酌.
5. 关于监听/依赖注入/路由等之类的问题请前往tp6看云文档仔细阅读.再此不做相关内容回答.
6. demo的说明文档在看云.[传送门](https://www.kancloud.cn/book_xwy/think-swoole3).